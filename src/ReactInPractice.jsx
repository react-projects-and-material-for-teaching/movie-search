import React, { useState, useEffect } from "react";

const PersonPropsExample = ({ name, lastName, age }) => {
  return (
    <>
      <p>Name: {name}</p>
      <p>Last Name: {lastName}</p>
      <p>Age: {age}</p>
    </>
  );
};

const StateExample = () => {
  // uses a concept of array destructuring
  const [counter, setCounter] = useState(0);

  useEffect(() => {
    alert("Reload");
    setCounter(100);
    // counter = 100 // never mutate state manually - react state can only be changed using its own setter function
  }, []); // if we dont add a dependancy array, useEffect will trigger everytime any state changes

  // Infinite loop - if we initially set the counter and keep changing it, it will continously run
  // useEffect renders below when the page loads, setCounter updated the counter value and then the counter recalls it and continously loops

  //   useEffect(() => {
  //     console.log("test");
  //     //   setCounter(100)
  //   }, [counter]);

  return (
    <>
      <h1>State in React</h1>
      {/* prev count(can be called anything) - is a paramter of the setState and  */}
      <button onClick={(prevCount) => setCounter(prevCount - 1)}>-</button>
      <h1>{counter}</h1>
      <button onClick={(prevCount) => setCounter(prevCount + 1)}>+</button>
    </>
  );
};

function ReactInPractice() {
  const name = "John";
  const isNameShowing = true;

  return (
    <div>
      <h1>Props in React</h1>
      <PersonPropsExample name="John" lastName="Doe" age={25} />
      <PersonPropsExample name="Bob" lastName="Dillon" age={50} />
      <hr />
      <StateExample />
      <hr />
      <h1>Conditionals in React</h1>
      {/* <h1>Hello, {name}</h1> */}
      <h4>Hello {isNameShowing ? name : "someone"}!</h4>
      {name ? (
        <p>displaying name</p>
      ) : (
        <>
          <p>Test</p>
          <p>not displaying name</p>
        </>
      )}
      <p>{2 + 2}</p>
    </div>
  );
}

export default ReactInPractice;
